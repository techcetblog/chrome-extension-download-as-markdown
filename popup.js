window.onload = function () {
    chrome.windows.getCurrent(function (currentWindow) {
        chrome.tabs.query({ active: true, windowId: currentWindow.id }, function (activeTabs) {
            chrome.tabs.executeScript(activeTabs[0].id, { file: "turndown.js", allFrames: true });
        });
    });

    let downloadBtn = document.getElementById("downloadMD");

    downloadBtn.addEventListener("click", function () {
        chrome.windows.getCurrent(function (currentWindow) {
            chrome.tabs.query({ active: true, windowId: currentWindow.id }, function (activeTabs) {
                chrome.tabs.executeScript(activeTabs[0].id, { file: "get_body.js", allFrames: true });
            });
        });
    });
};

var bodyTag = document.body.innerHTML;
var docTitle = document.title;

function downloadAsMd() {
    let turndownService = new TurndownService();
    let markdown = turndownService.turndown(bodyTag);

    //console.log(markdown);

    let data = new Blob([markdown], { type: "text/markdown" });
    let url = window.URL.createObjectURL(data);

    let anchor = document.createElement("a");
    anchor.setAttribute("download", docTitle + ".md");
    document.body.appendChild(anchor);
    anchor.href = url;
    anchor.click();
}

downloadAsMd();
